import re


def remove_links(x):
    p = re.sub("(?P<url>https?://[^\s]+)", "", x)
    return p


def get_number(x):
    a = re.findall('\d+', x)
    return a


def has_numbers(inputstring):
    return any(char.isdigit() for char in inputstring)


def remove_punc_generator(string):
    punc = '''!()-+[]{};':",./\<>?@#$%^&*~'''
    for ele in string:
        if ele in punc:
            string = string.replace(ele, " ")
    yield string


def remove_punc_number(string):
    number = []
    for ele in string:
        ele = ele.replace(" ", "")
        if '.' in ele:
            ele = ele.replace(ele, "")
        if ele.startswith("+84"):
            ele = ele.replace("+84", "0")
        if ele.startswith("84"):
            ele = ele.replace("84", "0")
        if not ele.startswith("0") and len(ele) > 6:
            ele = "0" + ele
        number.append(ele)
    return number


def check_number(x):
    list_numb = []
    for word in x.split(':'):
        if has_numbers(word):
            a = word.replace('.', '').replace(" ", "")
            z = get_number(a)
            for i in z:
                if len(i) > 14:
                    for ele in x.split(' '):
                        if has_numbers(ele):
                            for k in get_number(ele):
                                i = k
                            break
                list_numb.append(i)
    return list_numb


def find_zalo(item):
    item = remove_links(item)
    zalos = re.search('zalo', item.lower())
    zalo = None
    if zalos is not None:
        zalo_l = check_number(item)
        if zalo_l is not None and len(zalo_l) > 0:
            zalo_l = list(set(remove_punc_number(zalo_l)))
            zalo = [x for x in zalo_l if 8 < len(x) < 14]
    return zalo


def find_viber(item):
    item = remove_links(item)
    vibers = re.search('viber', item.lower())
    viber = None
    if vibers is not None:
        viber_l = check_number(item)
        if viber_l is not None and len(viber_l) > 0:
            viber_l = list(set(remove_punc_number(viber_l)))
            viber = [x for x in viber_l if 8 < len(x) < 14]
    return viber


def find_phone(item):
    item = remove_links(item)
    number_ab = None
    phones = check_number(item)
    if phones is not None and len(phones) > 0:
        number_ab = list(set(remove_punc_number(phones)))
        number_ab = [x for x in number_ab if 8 < len(x) < 14]
    return number_ab


def find_bank_account(line):
    item = remove_links(line)
    bank_account = None
    stk = re.search('stk', item.lower())
    string_banks = item.partition(":")[-1].replace('.', '').replace(' ', '')
    if '711A' in item:
        for word in item.split(' '):
            if '711A' in word:
                if len(word) > 12:
                    a = word.partition(":")[-1]
                    if len(a) == 12:
                        bank_account = [a]
                elif len(word) == 12:
                    bank_account = [word]
                break
    elif stk:
        bank_account = get_number(string_banks)
        if len(bank_account) > 0:
            bank_account = [x for x in bank_account if 6 < len(x)]
    else:
        stk = re.search('tk', item.lower())
        if stk:
            bank_account = get_number(string_banks)
            if len(bank_account) > 0:
                bank_account = [x for x in bank_account if 6 < len(x)]
        else:
            stk = re.search('tài khoản', item.lower())
            if stk:
                bank_account = get_number(string_banks)
                if len(bank_account) > 0:
                    bank_account = [x for x in bank_account if 6 < len(x)]
    return bank_account


def find_bank(item):
    bank = []
    for i in item.split("\n"):
        if 'bank' in i.lower():
            for word in i.split(' '):
                if 'bank' in word.lower():
                    word = next(remove_punc_generator(word))
                    for char in word.split(' '):
                        if 'bank' in char.lower():
                            bank.append(char.capitalize())
        elif 'vp bank' in i.lower():
            bank.append('Vpbank')
        elif 'tp bank' in i.lower():
            bank.append('Tpbank')
        elif 'shb' in i.lower():
            bank.append('SHB')
        elif 'vcb' in i.lower():
            bank.append('VCB')
        elif 'bidv' in i.lower():
            bank.append('BIDV')
        elif 'acb' in i.lower():
            bank.append('ACB')
        elif 'vib' in i.lower():
            bank.append('VIB')
        elif 'msb' in i.lower():
            bank.append('MSB')
        elif 'mb bank' in i.lower():
            bank.append('Mbbank')
    return bank
