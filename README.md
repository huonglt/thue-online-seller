# THUE-online-seller

B1. Lấy dữ liệu post từ server

- extract_post.py

Input: page post server 

Output: raw_post 

B2. Xử lí post data từ server

- process_post.py

Input: raw_post

Output: list_page_id/post_info

B3. Tìm thông tin của page

- extract_page.py

Input: list_page_id

Output: page_info 

B4. Merge data từ page và post

- merge.py 

Input: page_info & post_info

Output: final_page_info/sdt

B5: Tìm chủ shop và giải mã link rút gọn

- find_owner.py 

Input: sdt

Output: owner.json

- check_link.py 

Input: final_page_info

Output: final_page_info.json


TMDT

1. lazada.py 

Input: final_page_info.json

Output: lazada
