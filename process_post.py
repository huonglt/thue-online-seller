import itertools
import json

from utils import *


def extract_post(data):
    source = data.get("_source", None)
    page_id = source.get('pageId', '')
    messages = source.get('message', '')
    if messages:
        message = messages
    else:
        message = source.get('description', '')
    phone_about, bank_account, bank, zalos, vibers, instas = run_all(message)
    d = list(set(itertools.chain(phone_about, zalos, vibers)))
    phone = list(filter(None, d)) if 6 > len(list(filter(None, d))) > 0 else None
    zalo = zalos if len(zalos) > 0 else None
    viber = vibers if len(vibers) > 0 else None
    links = find_links(message)
    insta = None
    for i in instas:
        if i:
            insta = i
    df = {
        page_id: {'Phone': phone, 'Link In Post': links, 'STK': bank_account, 'Bank': bank, 'Insta': insta,
                  'Zalo': zalo, 'Viber': viber}}
    post = None
    if phone is not None or links is not None:
        post = df
    return post, page_id


def start_pool(pool, raw_page_post):
    data = raw_page_post.get('hits', {}).get('hits', None)
    for result, page_id in pool.imap_unordered(extract_post, data):
        if result and page_id:
            channel.basic_publish(exchange='', routing_key='post_info', body=json.dumps(result, ensure_ascii=False))
            channel.basic_publish(exchange='', routing_key='list_page_id', body=page_id)


if __name__ == '__main__':
    p = multiprocessing.Pool(cpu)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    for method_frame, properties, body in channel.consume('raw_post'):
        raw = body.decode()
        if raw == 'end_post':
            channel.basic_ack(method_frame.delivery_tag)
            break
        else:
            a = json.loads(raw)
            start_pool(p, a)
            print(method_frame.delivery_tag)
            channel.basic_ack(method_frame.delivery_tag)
    end = "end_post"
    channel.basic_publish(exchange='', routing_key='list_page_id', body=end)
    channel.basic_publish(exchange='', routing_key='post_info', body=end)
    channel.close()
    connection.close()
