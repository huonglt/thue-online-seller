import json
import logging
import multiprocessing

import pika
import requests
import urllib3.exceptions
from unshortenit import UnshortenIt

from utils import cpu, parameters

unshortener = UnshortenIt()


def remove_punc_generator(ele):
    if ele.endswith(']'):
        ele = ele.replace(']', '')
    elif ele.endswith(')'):
        ele = ele.replace(')', '')
    elif ele.endswith(','):
        ele = ele.replace(',', '')
    elif ele.endswith('.'):
        ele = ele.replace('.', '')
    return ele


def unshort(link):
    try:
        uri = unshortener.unshorten(link)
        return uri
    except requests.exceptions.ConnectionError:
        print("Connection refuse")
        print(link)
        return None
    except requests.exceptions.Timeout:
        print("Timeout occurred")
        print(link)
        return None
    except requests.exceptions.TooManyRedirects:
        print("Too many redirects")
        print(link)
        return None
    except requests.exceptions.ChunkedEncodingError:
        print("ChunkedEncodingError")
        print(link)
        return None
    except requests.exceptions.InvalidURL:
        print("InvalidURL")
        print(link)
        return None
    except urllib3.exceptions.LocationParseError:
        print("LocationParseError")
        print(link)
        return None
    except Exception as e:
        print("More")
        logging.exception(e)
        return None


def extract_link(data):
    if data['Links']:
        list_link = []
        for i in data['Links']:
            i = remove_punc_generator(i)
            uri = unshort(i)
            list_link.append(uri)
        data['Links'] = list_link
    return data


def check(pool, list_p):
    with open('data/output/final_page_info.json', 'a+', encoding='utf8') as f:
        for result in pool.imap_unordered(extract_link, list_p):
            print(result['Page ID'])
            json.dump(result, f, ensure_ascii=False)
            f.write('\n')


if __name__ == '__main__':
    p = multiprocessing.Pool(cpu)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    list_page = []
    for method_frame, properties, body in channel.consume('final_page_info'):
        raw = body.decode()
        if raw == 'end_post':
            channel.basic_ack(method_frame.delivery_tag)
            break
        else:
            page_info = json.loads(raw)
            list_page.append(page_info)
            channel.basic_ack(method_frame.delivery_tag)
    check(p, list_page)
    p.close()
    connection.close()
