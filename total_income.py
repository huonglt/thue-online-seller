from crawlShopee.crawlShopee import utils
# from crawlShopee.crawlShopee import utils
from datetime import datetime
import pandas as pd
import json
from elasticsearch import Elasticsearch,helpers

pd.set_option('display.max_columns', None)

# C:/Users/MSI 14RAS/PycharmProjects/shopee/crawlShopee/
path_products = 'C:/Users/MSI 14RAS/PycharmProjects/shopee/crawlShopee/product_list_sendo.csv'
path_info = 'C:/Users/MSI 14RAS/PycharmProjects/shopee/crawlShopee/sendo_shop_info.csv'
path_mapping = 'C:/Users/MSI 14RAS/PycharmProjects/shopee/crawlShopee/sendo_final_id.csv'
path_date = 'C:/Users/MSI 14RAS/PycharmProjects/shopee/crawlShopee/sendo_date.csv'
df = pd.read_csv(open(path_products, 'r', encoding='utf-8'))
df_info = pd.read_csv(open(path_info, 'r', encoding='utf-8'))
df_mapping = pd.read_csv(open(path_mapping, 'r', encoding='utf-8'))
df_date = pd.read_csv(open(path_date, 'r', encoding='utf-8'))
output = df.groupby(['Sendo_ID'])['revenue'].sum().reset_index(name='total_revenue')
df1 = df_info.merge(output, on="Sendo_ID", how='left')

df2 = df_mapping.merge(df1, on='Sendo_ID', how='left')
dfinal = df2.merge(df_date, on='Page ID', how='left')
years_has_joined = []
average_revenue = []

# dfinal["years_has_joined"] = utils.year_difference(dfinal, years_has_joined)
dfinal["average_revenue"] = utils.cal_average_revenue(dfinal, average_revenue)
# dfinal.to_string(index=False)
# dfinal.drop_duplicates(subset=None, keep="first", inplace=True)
# dfinal.to_json('C:/Users/MSI 14RAS/PycharmProjects/shopee/crawlShopee/sendo_total_revenue.json', )
with open('df.json', 'w', encoding='utf-8') as file:
    dfinal.to_json(file, orient='records', lines=True, force_ascii=False)
print(dfinal)

es = Elasticsearch(
    hosts="10.8.150.1",
    http_auth=("intern", "OcLcZzqjZMOT0npO"),
    port=9200,
)
files = open('df.json',encoding='utf-8')
index = 'dagoras_sendo_revenue_'
for line in files:
    a = json.loads(line)
    es.index(index = index, doc_type="_doc",id = (str(a['Page ID'])+'_'+ str(a['Sendo_ID'])).replace('.0',''),body=a)
    print(a)
