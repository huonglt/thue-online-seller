import json
import nltk
from nltk.stem import WordNetLemmatizer
import random
import numpy as np
from underthesea import word_tokenize
import pandas as pd

words=[]
classes = []
documents = []

ignore_words = ['?', '!', ","]

data_file = open('./data/'
                 'intents.json', encoding='utf-8').read()
intents = json.loads(data_file)

for intent in intents['intents']:
    for pattern in intent['patterns']:
        #add documents in the corpus
        w = word_tokenize(pattern)
        words.extend(w)
        documents.append((w, intent['tag']))
        # add to our classes list
        if intent['tag'] not in classes:
            classes.append(intent['tag'])


print("Classes: ", classes, "\n")
print("Words: ", words, "\n")
print("Documents: ", documents, "\n")


words = [w.lower() for w in words if w not in ignore_words]
words = sorted(list(set(words)))

print(words)

# # sort classes
classes = sorted(list(set(classes)))

# # words = all words, vocabulary
print (len(words), "unique lemmatized words", words)


# Create training and testing data

# create our training data
# create an empty array for our output
training = []
output_empty = [0] * len(classes)

# training set, bag of words for each sentence
for doc in documents:
    # initialize our bag of words
    bag = []
    # list of tokenized words for the pattern
    pattern_words = doc[0]
    # lemmatize each word - create base word, in attempt to represent related words
    pattern_words = [word.lower() for word in pattern_words]
    # create our bag of words array with 1, if word match found in current pattern
    for w in words:
        bag.append(1) if w in pattern_words else bag.append(0)
    # output is a '0' for each tag and '1' for current tag (for each pattern)
    output_row = list(output_empty)
    output_row[classes.index(doc[1])] = 1
    training.append([bag, output_row])


for t in training:
    print(t)

random.shuffle(training)
training = np.array(training, dtype=object)

# create train and test lists. X - patterns, Y - intents
X_train = list(training[:,0])
y_train = list(training[:,1])
print("Training data created")

for i in range(len(X_train)):
    print(X_train[i], y_train[i])

from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from tensorflow.keras.optimizers import SGD

# Create model - 3 layers. First layer 128 neurons, second layer 64 neurons and 3rd output layer contains number of neurons
# equal to number of intents to predict output intent with softmax
# model = Sequential()
# model.add(Dense(128*10, input_shape=(len(X_train[0]),), activation='relu'))
# model.add(Dropout(0.5))
# model.add(Dense(64*10, activation='relu'))
# model.add(Dropout(0.5))
# model.add(Dense(len(y_train[0]), activation='softmax'))
#
# # Compile model. Stochastic gradient descent with Nesterov accelerated gradient gives good results for this model
# sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
# model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
#
# #fitting and saving the model
# hist = model.fit(np.array(X_train), np.array(y_train), epochs=50, batch_size=5, verbose=1)
# model.save('model.h5', hist)
# print("model created")

from keras.models import load_model
model = load_model('model.h5')


def clean(sentence):
    # tokenize the pattern - split words into array
    sentence_words = word_tokenize(sentence, format='text')
    # stem each word - create short form for word
    sentence_words = [word.lower() for word in sentence_words]
    return sentence_words

# # return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
def bow(sentence, words):
    # tokenize the pattern
    sentence_words = clean(sentence)
    # bag of words - matrix of N words, vocabulary matrix
    bag = [0]*len(words)
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s:
                # assign 1 if current word is in the vocabulary position
                bag[i] = 1
    return(np.array(bag))
#
def predict_class(sentence, model):
    # filter out predictions below a threshold
    p = bow(sentence, words)
    res = model.predict(np.array([p]))[0]
    ERROR_THRESHOLD = 0.4
    print(res)

    results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]
    # sort by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)
    for r in results:
        label = (classes[r[0]])
    return label

while(True):
    w = str(input())
    print("You: ", w)
    res = predict_class(w, model)
    print("Bot: ",res)




